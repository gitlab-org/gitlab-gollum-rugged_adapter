# frozen_string_literal: true

module Gollum
  module Lib
    module Git
      VERSION = '0.4.4.4.gitlab.1'
    end
  end
end
