GitLab gollum rugged adapter

## This is a fork

This is a GitLab fork

[![Gem Version](https://badge.fury.io/rb/gitlab-gollum-rugged_adapter.svg)](http://badge.fury.io/rb/gitlab-gollum-rugged_adapter)
[![pipeline status](https://gitlab.com/gitlab-org/gitlab-gollum-rugged_adapter/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/gitlab-gollum-rugged_adapter/-/commits/master)

## Description

Adapter for [gollum](https://github.com/gollum/gollum) to use [Rugged](https://github.com/libgit2/rugged) (libgit2) at the backend. See the [gollum wiki](https://github.com/gollum/gollum/wiki/Git-adapters) for more information on adapters. Currently gollum uses grit as a backend by default, but since that is abandonware, the plan is to make this adapter the default in the future.

**Please note that this adapter is currently in beta. It passes the unit tests for gollum and [gollum-lib](https://github.com/gollum/gollum-lib), but it needs more comprehensive testing. Please [report any issues](https://github.com/gollum/rugged_adapter/issues) that you encounter.**

## Usage

Install the gem:

```bash
gem install --pre gollum-rugged_adapter # --pre required for beta-releases
```

Now run gollum as follows:

```bash
gollum --adapter rugged
```

## Contributing

1. Start by cloning the repo [on GitHub](http://github.com/gollum/rugged_adapter).
2. From inside the repo's directory, install the (development) dependencies with `bundle install`
3. Create a thoughtfully named topic branch to contain your changes.
4. Hack away.
5. Make sure your changes pass the adapter's specs: `bundle exec rake`
6. Make sure your changes pass gollum-lib's tests
  * Clone [gollum-lib](https://github.com/gollum/gollum-lib) and add your local version of the rugged adapter to the gollum-lib `Gemfile`:
  
    `gem "gollum-rugged_adapter", :path => '/path/to/rugged_adapter'`
  * `bundle install`
  * `bundle exec rake GIT_ADAPTER=rugged`
1. If necessary, rebase your commits into logical chunks, without errors.
1. Push the branch up to GitHub.
1. Send a pull request to the gollum/rugged_adapter project.

## Releasing a new GitLab fork version

This gem uses [Semantic Versioning](http://semver.org/).

1. Check the upstream gem at https://github.com/gollum/rugged_adapter and merge any needed changes.
1. Bump version in `lib/rugged_adapter/version.rb`.
   - Use the version of the upstream gem and add `.gitlab.$VERSION`, e.g. for the `1.0` version use `1.0.gitlab.1`.
1. Run `bundle install` to make sure `Gemfile.lock` is up-to-date.
1. Commit all changes.
1. Tag release and publish gem with `rake release`.
   - Use a personal work account for this.
